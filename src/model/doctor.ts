export interface IDoctorInfo {
    doctor: any,
    pubKey: string,
    signature: string,
    name?: string,
    peers: IPeer[],
}

export interface IPeer {
    s: string, // peer signature
    p: string, // peer pub key
}
