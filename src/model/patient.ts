import { IDoctorInfo } from './doctor';

export enum Sex {
    Male = 0,
    Female,
}

export enum MaritalStatus {
    Single = 0,
    Married = 1,
    Widow = 2,
    Separated = 3,
}

export interface IPatient {
    title: string,
    name: {
        suffix?: string,
        first: string,
        last: string,
        middle: string
        nick?: string,
    },
    sex: number,
    maritalStatus?: number,
    birthdate: string,
    language?: string,
    religion?: string,
    occupation?: string,
    city?: string,
    address?: string,
    contactMobile?: string,
    contactHome?: string,
    contactEmail?: string,
    contactOffice?: string,
    father?: string,
    mother?: string,
    guardian?: string,
    referredby?: string,
    allergies?: string,
    registration: string,
}

export interface IPatientCreds {
    _id?: string, // the id of the patient.
    k?: string, // the encryption key of the patient.
    patient?: IPatient,
    doctor?: IDoctorInfo
}

export interface IPatientPayload {
    _id: string, // the id of the patient.
    p: string, // the payload of the patient.
}

export interface IPatientSearched {
    dr: IDoctorInfo,
    patients: IPatientCreds[],
}
