export interface IPage {
    title: string,
    icon: string,
    component: any
}
