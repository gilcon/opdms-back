export interface IPostResponse {
    successful: boolean,
    msg: any,
}

export interface IGetResponse {
    successful: boolean,
    msg: any,
}
