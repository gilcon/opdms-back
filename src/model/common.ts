export interface ITitle {
    _id: string,
    _rev: string,
    title: string,
}

export interface IReligion {
    _id: string,
    _rev: string,
    religion: string,
}

export interface ILanguage {
    _id: string,
    _rev: string,
    language: string,
}
