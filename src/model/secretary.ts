export interface ISecretarySetting {
    _id: string,
    _rev: string,
    name: string,
    pubKey: string,
}
