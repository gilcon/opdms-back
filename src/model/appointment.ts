import { IDoctorInfo } from './doctor';
import { IPatientCreds } from './patient';

export interface IAppointmentList {
    dr: IDoctorInfo,
    apt: IPatientAppointment[],
}

export interface IPatientAppointmentView {
    doctor: IDoctorInfo,
    patientCreds: IPatientCreds,
    arrivalTime: string,
    bloodPressure: string,
    pulse: string,
    age: number,
    contact: string,
    referral: string,
    appointmentStatus: number,
    payable: number,
}

export interface IAppointmentVitalSign {
    arrivalTime?: string,
    scheduleStatus?: number, // -1 for completed
    bloodPressure?: string,
    pulse: string,
    weight?: number,
    height?: number,
    temp?: number,
    resp?: number,
    waist?: number,
    hip?: number,
    patientComplaint?: string,
    appointmentNotes?: string,
}


export interface IPatientAppointment {
    _id?: string, // the appointment id
    patient?: IPatientCreds,
    appointment?: IAppointmentVitalSign,
}
