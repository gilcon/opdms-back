import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { Keyboard } from '@ionic-native/keyboard'
import { ResponsiveModule } from 'ngx-responsive';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SecHomePage } from '../pages/sec-home/sec-home';
import { AuthPage } from '../pages/auth/auth';
import { RegistrationPage } from '../pages/registration/registration';
import { MedicinePage } from '../pages/medicine/medicine';
import { SettingPage } from '../pages/setting/setting';
import { PatientProfilePage } from '../pages/patient-profile/patient-profile';
import { DrSecSettingPopoverPage } from '../pages/popover/dr-sec-setting-popover/dr-sec-setting-popover';
import { AddRemovePopoverPage } from '../pages/popover/add-remove-popover/add-remove-popover';

import { UserProvider } from '../providers/user/user';
import { ServerAddressProvider } from '../providers/server-address/server-address';
import { StoreProvider } from '../providers/store/store';
import { LoggerProvider } from '../providers/logger/logger';
import { EncryptGcmProvider } from '../providers/encrypt-gcm/encrypt-gcm';
import { AppointmentProvider } from '../providers/appointment/appointment';
import { PatientProvider } from '../providers/patient/patient';
import { CommonProvider } from '../providers/common/common';
import { AgeProvider } from '../providers/age/age';
import { TitlesSettingProvider } from '../providers/settings/titles-setting/titles-setting';
import { ReligionsSettingProvider } from '../providers/settings/religions-setting/religions-setting';
import { LanguagesSettingProvider } from '../providers/settings/languages-setting/languages-setting';
import { PatientSearchProvider } from '../providers/patient-search/patient-search';

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        SecHomePage,
        AuthPage,
        RegistrationPage,
        MedicinePage,
        SettingPage,
        PatientProfilePage,
        // popovers
        DrSecSettingPopoverPage,
        AddRemovePopoverPage,
    ],
    imports: [
        BrowserModule,
        ResponsiveModule.forRoot(),
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot({
            name: '__OPDMSStorage',
            driverOrder: ['indexeddb', 'sqlite', 'websql'],
        }),
        HttpClientModule,
        BrMaskerModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        SecHomePage,
        AuthPage,
        RegistrationPage,
        MedicinePage,
        SettingPage,
        PatientProfilePage,
        // popovers
        DrSecSettingPopoverPage,
        AddRemovePopoverPage,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Keyboard,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        UserProvider,
        ServerAddressProvider,
        StoreProvider,
        LoggerProvider,
        EncryptGcmProvider,
        AppointmentProvider,
        PatientProvider,
        CommonProvider,
        AgeProvider,
        TitlesSettingProvider,
        ReligionsSettingProvider,
        LanguagesSettingProvider,
        PatientSearchProvider,
    ]
})

export class AppModule { }
