import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { SecHomePage } from '../pages/sec-home/sec-home';
import { AuthPage } from '../pages/auth/auth';
import { MedicinePage } from '../pages/medicine/medicine';
import { SettingPage } from '../pages/setting/setting';

import { UserProvider, UserType } from '../providers/user/user';
import { StoreProvider } from '../providers/store/store';

import { IPage } from '../model/page';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    public rootPage: any;

    public pages: Array<IPage>;
    public activePage: IPage;


    constructor(
        private _user: UserProvider,
        private _store: StoreProvider,
        private _events: Events,
        private _menu: MenuController,
        public platform: Platform,
        public statusBar: StatusBar,
        public splashScreen: SplashScreen
    ) {
        this._menu.enable(false, 'main-menu');
        this._store.initMonitor();

        platform.ready().then(() => {
            statusBar.styleDefault();
            splashScreen.hide();

            _events.subscribe('user:session', session => {
                if (session) {
                    this.initStores(session);
                    this.initServers();

                    if (_user.userType === UserType.DOCTOR) {
                        this.initPages();
                        this.rootPage = HomePage
                    } else if (_user.userType === UserType.SECRETARY) {
                        this.initSecPage();
                        this.rootPage = SecHomePage;
                    }

                    this._menu.enable(true, 'main-menu');
                } else {
                    this.pages = [];
                    this._menu.enable(false, 'main-menu');
                    this.rootPage = AuthPage;
                }
            })

            this._user.signalAuth();
        });

    }

    private initStores(session) {
        this._events.publish('store:location', session.ps);
        this._events.publish('store:location', session.aps);
        this._events.publish('store:location', session.vs);

        this._user.patientStore[session.signature] = session.ps;
        this._user.appointmentStore[session.signature] = session.aps;
        this._user.visitStore = session.vs;
    }

    private initServers() {
    }

    private initPages() {
        this.pages = [
            { title: 'Home', icon: 'home', component: HomePage },
            { title: 'Medicines', icon: 'medkit', component: MedicinePage },
            { title: 'Settings', icon: 'settings', component: SettingPage }
        ];

        this.activePage = this.pages[0];
    }

    private initSecPage() {
        this.pages = [
            { title: 'Home', icon: 'home', component: HomePage },
            { title: 'Settings', icon: 'settings', component: SettingPage },
        ];

        this.activePage = this.pages[0];
    }

    public openPage(page: IPage) {
        this.nav.setRoot(page.component);
        this.activePage = page;
    }

    public getActivePage(page: IPage) {
        return this.activePage === page;
    }

    public logout() {
        this._user.logout();
    }
}
