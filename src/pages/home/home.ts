import { Component, ViewChild } from '@angular/core';
import { Events, NavController, NavParams, AlertController, IonicPage, Searchbar } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';

import { UserProvider } from '../../providers/user/user';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { StoreProvider } from '../../providers/store/store';
import { EncryptGcmProvider } from '../../providers/encrypt-gcm/encrypt-gcm';
import { LoggerProvider } from '../../providers/logger/logger';
import { AgeProvider } from '../../providers/age/age';
import { IDoctorInfo } from '../../model/doctor';
import { PatientProvider } from '../../providers/patient/patient';
import { CommonProvider } from '../../providers/common/common';
import { PatientSearchProvider } from '../../providers/patient-search/patient-search';
import { Sex, MaritalStatus, IPatientCreds } from '../../model/patient';
import { PatientProfilePage } from '../patient-profile/patient-profile';

interface IClinicNode {
    node: string,
    alias: string
}

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    @ViewChild('patientSearchBar') patientSearchBar: Searchbar

    public Sex = Sex;
    public MaritalStatus = MaritalStatus;

    public dInf: IDoctorInfo;

    public nodes: any = undefined;
    public selectedNode: IClinicNode = undefined;

    constructor(
        private _enc: EncryptGcmProvider,
        private _sp: StoreProvider,
        private _log: LoggerProvider,
        private _ls: Storage,
        private _alertCtrl: AlertController,
        private _events: Events,
        public user: UserProvider,
        public pt: PatientProvider,
        public apt: AppointmentProvider,
        public ptSearch: PatientSearchProvider,
        public age: AgeProvider,
        public common: CommonProvider,
        public navCtrl: NavController,
        public navParams: NavParams
    ) {
        this._initNodes();
        this._initIDoctorInfo();

        let i = setInterval(() => {
            if (this.dInf && this.selectedNode) {
                this.apt.initData(moment(), this.dInf, this.selectedNode.node);
                clearInterval(i);
            }
        }, 1000);
    }

    public changeNode() {
        let lCtrl = [];

        for (let i = 0; i < this.nodes.nodes.length; i++) {
            let r = {
                type: 'radio',
                label: this.nodes[this.nodes.nodes[i]],
                value: this.nodes.nodes[i],
                checked: this.nodes.nodes[i] === this.selectedNode.node
            }

            lCtrl.push(r);
        }

        this._alertCtrl.create({
            title: 'Select Location',
            inputs: lCtrl,
            buttons: [{
                text: 'Cancel',
                role: 'cancel'
            }, {
                text: 'OK',
                handler: data => {
                    this.selectedNode = {
                        node: data,
                        alias: this.nodes[data]
                    }
                }
            }]
        }).present();
    }

    private _initIDoctorInfo() {
        let cUser = this.user.currentSession;
        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .get(`peers${cUser.signature}`).then(res => {
                this.dInf = {
                    doctor: cUser,
                    pubKey: cUser.pubKey,
                    signature: cUser.signature,
                    peers: res.peers
                };
            });
    }

    private _clearPatientSearchBar() {
        this._events.unsubscribe('pProfile:close', this._clearPatientSearchBar);
        this.patientSearchBar.clearInput(null);
    }

    private _initNodes() {
        let st = this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii');

        st.get(`node:${this.user.signature}`).then(doc => {
            this.nodes = JSON.parse(this._enc.decrypt(doc.list,
                this.user.currentSession.uuid));

            this._sp.fetchServerUUID().then(uuid => {

                if (!this.nodes[uuid]) {
                    this._alertCtrl.create({
                        title: 'Server First Logged-in',
                        subTitle: 'Please provide alias to identify this server.',
                        enableBackdropDismiss: false,
                        inputs: [{
                            name: 'alias',
                            placeholder: 'Server Alias'
                        }],
                        buttons: [{
                            text: 'OK',
                            handler: data => {
                                this.nodes.nodes.push(uuid);
                                this.nodes[uuid] = data.alias;

                                this.selectedNode = {
                                    node: uuid,
                                    alias: data.alias
                                }

                                this._ls.set(
                                    'user:selectednode',
                                    this.selectedNode
                                );

                                st.get(`node:${this.user.signature}`)
                                    .then(doc => {
                                        doc.list = JSON.stringify(
                                            this._enc.encrypt(
                                                JSON.stringify(this.nodes),
                                                this.user.currentSession.uuid
                                            )
                                        );

                                        return st.put(doc);
                                    })
                                    .catch(e => {
                                        this._log.log(e);
                                    });
                            }
                        }]
                    }).present();
                } else {
                    this.selectedNode = {
                        node: uuid,
                        alias: this.nodes[uuid]
                    };

                    this._ls.set('user:selectednode', this.selectedNode);
                }
            }).catch(e => {
                this._ls.get('user:selectednode').then(node => {
                    if (!node) {
                        return this.user.logout();
                    }

                    this.selectedNode = node;
                }).catch(e => {
                    this.user.logout();
                    this._log.log(e);
                })
                this._log.log(e);
            })
        }).catch(e => {
            this._log.log(e);
        });
    }

    public patientOpen(pc: IPatientCreds) {
        this.navCtrl.push(PatientProfilePage, {
            pc, apt: {}, node: this.selectedNode.node
        }).then(() => {
            this._events.subscribe('pProfile:close',
                this._clearPatientSearchBar.bind(this));
        });
    }
}
