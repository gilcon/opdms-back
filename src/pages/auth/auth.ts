import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { RegistrationPage } from '../registration/registration';

import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
    selector: 'page-auth',
    templateUrl: 'auth.html',
})
export class AuthPage {

    public loginForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private user: UserProvider,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        public navCtrl: NavController,
        public navParams: NavParams
    ) {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    ionViewDidLoad() {
    }


    public login() {
        if (!this.loginForm.valid) {
            return;
        }

        let l = this.loadingCtrl.create({
            content: 'Logging in...'
        });
        l.present();

        this.user.login(
            this.loginForm.controls['username'].value,
            this.loginForm.controls['password'].value
        ).then(successful => {
            if (!successful) {
                l.onDidDismiss(() => {
                    this.loginForm.controls['username'].reset();
                    this.loginForm.controls['password'].reset();

                    this.alertCtrl.create({
                        title: 'Authentication',
                        subTitle: 'Invalid username or password.',
                        buttons: ['OK']
                    }).present();
                })
            } else {
                this.loginForm.controls['username'].reset();
                this.loginForm.controls['password'].reset();
            }
            l.dismiss();
        }).catch(e => {
            l.onDidDismiss(() => {
                this.alertCtrl.create({
                    title: 'Authentication',
                    subTitle: 'Unable to connect to the server.' +
                        '<br><br>Please check your connectivity and try again.',
                    buttons: ['OK']
                }).present();

                this.loginForm.controls['username'].reset();
                this.loginForm.controls['password'].reset();
            });
            l.dismiss();
        });
    }

    public register() {
        this.navCtrl.push(RegistrationPage);
    }
}
