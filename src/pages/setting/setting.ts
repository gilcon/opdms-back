import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

import { UserProvider, UserType } from '../../providers/user/user';
import { StoreProvider } from '../../providers/store/store';
import { LoggerProvider } from '../../providers/logger/logger';
import { EncryptGcmProvider } from '../../providers/encrypt-gcm/encrypt-gcm';


import { DrSecSettingPopoverPage } from '../../pages/popover/dr-sec-setting-popover/dr-sec-setting-popover';

import { ISecretarySetting } from '../../model/secretary';
import { TitlesSettingProvider } from '../../providers/settings/titles-setting/titles-setting';
import { LanguagesSettingProvider } from '../../providers/settings/languages-setting/languages-setting';
import { ReligionsSettingProvider } from '../../providers/settings/religions-setting/religions-setting';

@IonicPage()
@Component({
    selector: 'page-setting',
    templateUrl: 'setting.html',
})
export class SettingPage {

    public isDoctor: boolean = false;
    public secList: ISecretarySetting[] = [];

    constructor(
        private _user: UserProvider,
        private _sp: StoreProvider,
        private _log: LoggerProvider,
        private _enc: EncryptGcmProvider,
        public _titles: TitlesSettingProvider,
        public _langs: LanguagesSettingProvider,
        public _religions: ReligionsSettingProvider,
        public popoverCtrl: PopoverController,
        public navCtrl: NavController,
        public navParams: NavParams
    ) {

        this.isDoctor = this._user.userType === UserType.DOCTOR;
    }

    ionViewDidLoad() {
        this._listSecretariest();
        this._initTitles();
        this._initReligions();
        this._initLanguages();
    }

    private _listSecretariest() {
        this.secList = [];
        let s = this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii');

        s.allDocs({
            include_docs: true,
            startkey: `s${this._user.currentSession.signature}`,
            endkey: `s${this._user.currentSession.signature}\ufff0`
        }).then(res => {
            let rows = res.rows;

            for (let i = 0; i < rows.length; i++) {
                let doc = rows[i].doc;
                if (doc.error) continue;
                let sec = JSON.parse(this._enc.decrypt(doc.i, this._user.currentSession.uuid));

                this.secList.push({
                    _id: doc._id,
                    _rev: doc._rev,
                    name: [sec.name.first, sec.name.last].join(' '),
                    pubKey: doc.p
                });
            }
        }).catch(e => {
            this._log.log(e);
        });
    }

    public secretaryOptions(event) {
        let p = this.popoverCtrl.create(DrSecSettingPopoverPage, {
            secList: this.secList.filter(s => { return s['_deleted'] })
        });

        p.present({ ev: event });
        p.onDidDismiss(reset => {
            if (reset) {
                this._listSecretariest();
            }
        })
    }

    private _initTitles() {
        this._titles.fetchTitles();
    }

    public titlePopover(event) {
        this._titles.popOver(event);
    }

    private _initReligions() {
        this._religions.fetchReligions();
    }

    public religionPopover(event) {
        this._religions.popOver(event);
    }

    private _initLanguages() {
        this._langs.fetchLang();
    }

    public langPopover(event) {
        this._langs.popOver(event);
    }
}
