import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';

import { PatientProvider } from '../../providers/patient/patient';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { IPatient, IPatientCreds } from '../../model/patient';
import { IAppointmentVitalSign, IPatientAppointment } from '../../model/appointment';
import { CommonProvider } from '../../providers/common/common';
import { TitlesSettingProvider } from '../../providers/settings/titles-setting/titles-setting';
import { LanguagesSettingProvider } from '../../providers/settings/languages-setting/languages-setting';
import { ReligionsSettingProvider } from '../../providers/settings/religions-setting/religions-setting';

@IonicPage()
@Component({
    selector: 'page-patient-profile',
    templateUrl: 'patient-profile.html'
})
export class PatientProfilePage {

    private _pc: IPatientCreds;
    private _apt: IPatientAppointment;

    public node: string;
    public patientProfileForm: FormGroup;
    public additionalInfo: any;
    public medicalProfileInfo: any;

    public aptComplete: any = undefined;
    public apptId: any = undefined;

    constructor(
        private _fb: FormBuilder,
        private _alertCtrl: AlertController,
        private _pp: PatientProvider,
        private _ap: AppointmentProvider,
        private _events: Events,
        public _titles: TitlesSettingProvider,
        public _langs: LanguagesSettingProvider,
        public _religions: ReligionsSettingProvider,
        public common: CommonProvider,
        public navCtrl: NavController,
        public navParams: NavParams
    ) {
        this._pc = this.navParams.get('pc');
        this._apt = this.navParams.get('apt');
        this.node = this.navParams.get('node');

        this._titles.fetchTitles();
        this._religions.fetchReligions();
        this._langs.fetchLang();

        this.apptId = this._apt._id;
        if (this._apt.appointment) {
            this.aptComplete = this._apt.appointment.scheduleStatus ===
                -1 ? -1 : undefined;
        }

        this._initPatientProfile();

        this.additionalInfo = 'contact';
        this.medicalProfileInfo = 'vital-sign';
    }

    ionViewDidLoad() {
    }

    ionViewCanLeave(): Promise<boolean> {
        return new Promise(resolve => {
            if (!this.patientProfileForm.dirty) return resolve(true);

            this._alertCtrl.create({
                title: 'Confirm leaving',
                message: 'There is unsave work, do you like to continue leaving?',
                buttons: [{
                    text: 'Leave',
                    handler: () => {
                        resolve(true);
                    }
                }, {
                    text: 'Stay',
                    handler: () => {
                        resolve(false);
                    }
                }]
            }).present();
        });
    }

    private _initPatientProfile() {
        let pt: IPatient = this.navParams.get('pc').patient || {};
        let vital: IAppointmentVitalSign = this.navParams
            .get('apt').appointment || {};

        let arrival = !vital.arrivalTime ? moment().format() :
            moment(vital.arrivalTime).format();

        pt.name = pt.name || { first: '', last: '', middle: '' };

        this.patientProfileForm = this._fb.group({
            additionalInfo: [''],
            medicalProfileInfo: [''],
            title: [pt.title || '', Validators.required],
            suffixname: [pt.name.suffix || ''],
            firstname: [
                pt.name.first || '',
                Validators.compose([
                    Validators.required,
                    Validators.pattern('[a-zA-Z ]*')
                ])
            ],
            lastname: [
                pt.name.last || '',
                Validators.compose([
                    Validators.required,
                    Validators.pattern('[a-zA-Z ]*')
                ])
            ],
            middlename: [pt.name.middle || '', Validators.pattern('[a-zA-Z ]*')],
            nickname: [pt.name.nick || ''],
            sex: [pt.sex || '', Validators.required],
            maritalStatus: [pt.maritalStatus || '0'],
            birthdate: [pt.birthdate || '', Validators.required],
            language: [pt.language || ''],
            religion: [pt.religion || ''],
            occupation: [pt.occupation || ''],
            city: [pt.city || ''],
            address: [pt.address || ''],
            contactMobile: [pt.contactMobile || ''],
            contactHome: [pt.contactHome || ''],
            contactEmail: [pt.contactEmail || ''],
            contactOffice: [pt.contactOffice || ''],
            father: [pt.father || ''],
            mother: [pt.mother || ''],
            guardian: [pt.guardian || ''],
            referredby: [pt.referredby || ''],
            allergies: [pt.allergies || ''],
            arrivalTime: [arrival],
            scheduleStatus: [vital.scheduleStatus || '0'],
            bloodPressure: [vital.bloodPressure || ''],
            weight: [vital.weight || ''],
            height: [vital.height || ''],
            pulse: [vital.pulse || ''],
            temp: [vital.temp || ''],
            resp: [vital.resp || ''],
            waist: [vital.waist || ''],
            hip: [vital.hip || ''],
            patientComplaint: [vital.patientComplaint || ''],
            appointmentNotes: [vital.appointmentNotes || '']
        });

        if (vital.scheduleStatus < 0) {
            this.patientProfileForm.controls['scheduleStatus'].disable();
        }
    }

    isFieldValid(field: string) {
        return !this.patientProfileForm.get(field).valid &&
            this.patientProfileForm.get(field).touched;
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    public patientSave(appointment: boolean) {
        if (this.patientProfileForm.valid) {
            if (!this._pc._id || this.patientProfileForm.dirty) {
                this._pc.patient = this._getPatientInfo();
                this._pp.save(this._pc);
            }

            if (appointment || (this._apt._id && this.patientProfileForm.dirty)) {
                this._apt.patient = this._pc;
                this._apt.appointment = this._getPatientAppointment();
                this._ap.save(this._apt, this.node);
            }

            this.patientProfileForm.markAsPristine();
            this.navCtrl.pop().then(() => {
                this._events.publish('pProfile:close');
            });
        } else {
            this.validateAllFormFields(this.patientProfileForm);
            this._alertCtrl.create({
                title: 'Patient Registration',
                subTitle: 'Some fields has missing/invalid value.' +
                    '<br><br>Please check, and try again.',
                buttons: ['OK']
            }).present();
        }
    }

    private _getPatientInfo(): IPatient {
        let v = this.patientProfileForm.value;

        return {
            title: v.title,
            name: {
                suffix: v.suffixname,
                first: v.firstname,
                last: v.lastname,
                middle: v.middlename,
                nick: v.nickname
            },
            sex: v.sex,
            maritalStatus: v.maritalStatus,
            birthdate: v.birthdate,
            language: v.language,
            religion: v.religion,
            occupation: v.occupation,
            city: v.city,
            address: v.address,
            contactMobile: v.contactMobile,
            contactHome: v.contactHome,
            contactEmail: v.contactEmail,
            contactOffice: v.contactOffice,
            father: v.father,
            mother: v.mother,
            guardian: v.guardian,
            referredby: v.referredby,
            allergies: v.allergies,
            registration: this._pc.patient ? this._pc.patient.registration :
                moment().format('YYYY-MM-DD'),
        }
    }

    private _getPatientAppointment(): IAppointmentVitalSign {
        let v = this.patientProfileForm.value;

        return {
            arrivalTime: v.arrivalTime,
            scheduleStatus: v.scheduleStatus,
            bloodPressure: v.bloodPressure,
            pulse: v.pulse,
            weight: v.weight,
            height: v.height,
            temp: v.temp,
            resp: v.resp,
            waist: v.waist,
            hip: v.hip,
            patientComplaint: v.patientComplaint,
            appointmentNotes: v.appointmentNotes
        }
    }
}
