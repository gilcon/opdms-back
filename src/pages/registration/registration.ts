import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { UserProvider } from '../../providers/user/user';


@IonicPage()
@Component({
    selector: 'page-registration',
    templateUrl: 'registration.html',
})
export class RegistrationPage {

    public registrationForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private alertCtrl: AlertController,
        private loadingCtrl: LoadingController,
        public navCtrl: NavController,
        public navParams: NavParams,
        private user: UserProvider
    ) {
        //https://embed.plnkr.co/ukwCXm/
        this.registrationForm = this.formBuilder.group({
            username: [
                '',
                Validators.compose([
                    Validators.required
                ]), this.validateUsername.bind(this)
            ],
            userType: [2, Validators.required],
            password: [
                '',
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8)
                ])
            ],
            repassword: ['', Validators.compose([Validators.required])],
            firstname: [
                '',
                Validators.compose([
                    Validators.required,
                    Validators.pattern('[a-zA-Z ]*')
                ])
            ],
            lastname: [
                '',
                Validators.compose([
                    Validators.required,
                    Validators.pattern('[a-zA-Z ]*')
                ])
            ],
            middlename: ['', Validators.pattern('[a-zA-Z ]*')],
            suffixname: ['', Validators.pattern('[a-zA-Z ]*')],
            contactNumber: [''],
            address: [''],
            question1: ['', Validators.required],
            answer1: ['', Validators.required],
            question2: ['', Validators.required],
            answer2: ['', Validators.required]
        }, {
                validator: this.matchingPassword('password', 'repassword')
            }
        );
    }


    ionViewDidLoad() {
    }

    private matchingPassword(password: string, repassword: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let p = group.controls[password];
            let r = group.controls[repassword];

            if (p.value !== r.value) {
                return { mismatchedPassword: true };
            }
        };
    }

    //https://alligator.io/angular/async-validators/
    private validateUsername(control: FormControl) {
        return this.user.usernameExists(control.value).map(res => {
            return res['exists'] ? { exists: true } : null;
        });
    }


    public register() {
        if (this.registrationForm.valid) {
            let l = this.loadingCtrl.create({
                content: 'Please wait...<br>Registration is on progress'
            });

            l.present();
            this.user.register(this.registrationForm.value).subscribe(res => {
                l.dismiss();
                if (!res['success']) {
                    this.alertCtrl.create({
                        title: 'Registration Error',
                        subTitle: res['msg'],
                        buttons: ['OK']
                    }).present();
                } else {
                    let alt = this.alertCtrl.create({
                        title: 'Registration',
                        subTitle: 'Successfully registered',
                        buttons: ['OK']
                    });
                    alt.present();
                    alt.onDidDismiss(() => {
                        this.navCtrl.pop();
                    });
                }
            });
        } else {
            this.alertCtrl.create({
                title: 'Registration Error',
                subTitle: 'Some fields has missing/invalid value.' +
                    '<br><br>Please check, and try again.',
                buttons: ['OK']
            }).present();
        }
    }
}
