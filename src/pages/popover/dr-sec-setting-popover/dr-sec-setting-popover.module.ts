import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrSecSettingPopoverPage } from './dr-sec-setting-popover';

@NgModule({
    declarations: [
        DrSecSettingPopoverPage,
    ],
    imports: [
        IonicPageModule.forChild(DrSecSettingPopoverPage),
    ],
})
export class DrSecSettingPopoverPageModule { }
