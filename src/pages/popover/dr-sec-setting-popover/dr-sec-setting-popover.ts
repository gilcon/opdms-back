import { Component } from '@angular/core';
import { AlertController, IonicPage, NavParams, ViewController } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';
import { ISecretarySetting } from '../../../model/secretary';

@IonicPage()
@Component({
    selector: 'page-dr-sec-setting-popover',
    templateUrl: 'dr-sec-setting-popover.html',
})
export class DrSecSettingPopoverPage {

    constructor(
        private _user: UserProvider,
        private _alertCtrl: AlertController,
        public viewCtrl: ViewController,
        public navParams: NavParams
    ) { }

    public addSec() {
        this._alertCtrl.create({
            title: 'Add Secretrary',
            inputs: [{
                name: 'username',
                placeholder: 'Username'
            }, {
                name: 'password',
                type: 'password',
                placeholder: 'Password'
            }],
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    this.viewCtrl.dismiss(false);
                }
            }, {
                text: 'Add',
                handler: data => {
                    this._user.addSecretary(data.username, data.password)
                        .then(res => {
                            if (res) {
                                this.viewCtrl.dismiss(true);
                            } else {
                                this._alertCtrl.create({
                                    title: 'Secretary login Failed',
                                    subTitle: `
Failed to add new secretary.<br><br>Please try again.
`,
                                    buttons: ['OK']
                                }).present();

                                this.viewCtrl.dismiss(true);
                            }
                        });
                }
            }]
        }).present();
    }
    public rmSel() {
        let secList: Array<ISecretarySetting> = this.navParams.get('secList');
        if (secList.length > 0) {
            let secs = [];
            let message = 'Are you sure to remove selected secretary?<br>';

            for (let i = 0; i < secList.length; i++) {
                secs.push(secList[i].name);
            }

            this._alertCtrl.create({
                title: 'Remove Selected Secretary',
                message: message + secs.join('<br>'),
                buttons: [{
                    text: 'Cancel',
                    handler: () => {
                        this.viewCtrl.dismiss(false);
                    }
                }, {
                    text: 'Confirm',
                    handler: () => {
                        this._user.removeSecretary(secList);
                        this.viewCtrl.dismiss(true);
                    }
                }]
            }).present();
        } else {
            this.viewCtrl.dismiss(false);
        }
    }
}
