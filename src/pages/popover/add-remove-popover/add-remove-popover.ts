import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-add-remove-popover',
    templateUrl: 'add-remove-popover.html',
})
export class AddRemovePopoverPage {
    public showAdd: boolean;
    public showRemove: boolean;
    public showUpdate: boolean;

    public addText: string = 'Add';
    public removeText: string = 'Remove';
    public updateText: string = 'Update';

    public addCB: (ard: AddRemovePopoverPage) => void;
    public rmCB: (ard: AddRemovePopoverPage) => void;
    public upCB: (ard: AddRemovePopoverPage) => void;

    constructor(
        public viewCtrl: ViewController,
        public navParams: NavParams
    ) {
        this.addCB = this.navParams.get('addCB');
        this.rmCB = this.navParams.get('rmCB');
        this.upCB = this.navParams.get('upCB');

        this.showAdd = this.navParams.get('showAdd') || true;
        this.showRemove = this.navParams.get('showRemove') || true;
        this.showUpdate = this.navParams.get('showUpdate') || false;
    }

    public add() {
        if (!this.addCB) {
            return;
        }

        this.addCB(this);
    }

    public rm() {
        if (!this.rmCB) {
            return;
        }

        this.rmCB(this);
    }

    public update() {
        if (!this.upCB) {
            return;
        }

        this.upCB(this);
    }

}
