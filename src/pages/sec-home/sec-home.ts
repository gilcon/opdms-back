import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events, Searchbar } from 'ionic-angular';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';

import { PatientProfilePage } from '../patient-profile/patient-profile';

import { UserProvider, UserType } from '../../providers/user/user';
import { StoreProvider } from '../../providers/store/store';
import { LoggerProvider } from '../../providers/logger/logger';
import { EncryptGcmProvider } from '../../providers/encrypt-gcm/encrypt-gcm';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { PatientProvider } from '../../providers/patient/patient';
import { IDoctorInfo } from '../../model/doctor';
import { IPatientCreds, Sex, MaritalStatus } from '../../model/patient'
import { CommonProvider } from '../../providers/common/common';
import { AgeProvider } from '../../providers/age/age';
import { IPatientAppointment } from '../../model/appointment';
import { PatientSearchProvider } from '../../providers/patient-search/patient-search';

@IonicPage()
@Component({
    selector: 'page-sec-home',
    templateUrl: 'sec-home.html',
})
export class SecHomePage {
    @ViewChild('patientSearchBar') patientSearchBar: Searchbar

    public Sex = Sex;
    public MaritalStatus = MaritalStatus;

    public UserType = UserType;
    public subjectDate: moment.Moment = moment();
    public node: string = '';

    public doctorList: IDoctorInfo[] = [];
    public selectedDoctor: IDoctorInfo = this.common.emptyDoctor;

    constructor(
        private _sp: StoreProvider,
        private _log: LoggerProvider,
        private _enc: EncryptGcmProvider,
        private _user: UserProvider,
        private _events: Events,
        private _ls: Storage,
        private _alertCtrl: AlertController,
        public pt: PatientProvider,
        public apt: AppointmentProvider,
        public ptSearch: PatientSearchProvider,
        public age: AgeProvider,
        public common: CommonProvider,
        public navCtrl: NavController,
        public navParams: NavParams
    ) {
        this._getClinicNode(node => {
            this.node = node;
            this._getDoctorList();
        });
    }

    ionViewDidLoad() {
    }

    private _getClinicNode(callback: (uuid: string) => void) {
        this._ls.get('user:clinic:node').then((node: string) => {
            if (!node) {
                this._sp.fetchServerUUID().then(db => {
                    this._ls.set('user:clinic:node', db['uuid']).then(() => {
                        callback(db['uuid'])
                    });
                })
            } else {
                callback(node)
            }
        });
    }

    public changeDoctor() {
        let dCtrl = [];

        for (let i = 0; i < this.doctorList.length; i++) {
            let d = {
                type: 'radio',
                label: this.doctorList[i].name,
                value: this.doctorList[i],
                checked: this.selectedDoctor.signature ===
                    this.doctorList[i].signature
            };

            dCtrl.push(d);
        }

        this._alertCtrl.create({
            title: 'Select Doctor',
            inputs: dCtrl,
            buttons: [{
                text: 'Cancel',
                role: 'cancel'
            }, {
                text: 'OK',
                handler: data => {
                    this.selectedDoctor = data;
                }
            }]
        }).present();
    }

    private _getDoctorList() {
        let s = this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii');
        s.allDocs({
            include_docs: true,
            startkey: `d${this._user.signature}`,
            endkey: `d${this._user.signature}\ufff0`
        }).then(res => {
            let rows = res.rows;

            this.doctorList.push(this.common.emptyDoctor);

            for (let i = 0; i < rows.length; i++) {
                if (rows[i].error) continue;
                s.get(`peers${rows[i].doc.s}`).then(doc => {
                    let dr: IDoctorInfo = {
                        doctor: JSON.parse(this._enc.decrypt(
                            rows[i].doc.i,
                            this._user.currentSession.uuid
                        )),
                        pubKey: rows[i].doc.p,
                        signature: rows[i].doc.s,
                        peers: doc.peers
                    };

                    dr.name = 'DR. ' + dr.doctor.name.first.substring(0, 1)
                        + '. ' + dr.doctor.name.last;

                    this.doctorList.push(dr);
                    this._sp.get(dr.doctor.pes);
                    this._sp.get(dr.doctor.aps);
                    this._sp.get(dr.doctor.ps);
                    this._sp.get(dr.doctor.pti);

                    this.apt.initData(moment(), dr, this.node);
                })
            }
        }).catch(e => {
            this._log.log(e);
        });
    }

    private _clearPatientSearchBar() {
        this._events.unsubscribe('pProfile:close', this._clearPatientSearchBar);
        this.patientSearchBar.clearInput(null);
    }

    public newPatient() {
        let showProfPage = (dr: IDoctorInfo) => {
            this.navCtrl.push(PatientProfilePage, {
                pc: { doctor: dr }, apt: {}, node: this.node
            }).then(() => {
                this._events.subscribe('pProfile:close',
                    this._clearPatientSearchBar.bind(this));
            })
        }


        if (this.selectedDoctor === this.common.emptyDoctor &&
            this.doctorList.length > 1) {
            let dCtrl = [];

            for (let i = 0; i < this.doctorList.length; i++) {
                if (this.doctorList[i] === this.common.emptyDoctor) continue;

                dCtrl.push({
                    type: 'radio',
                    label: this.doctorList[i].name,
                    value: this.doctorList[i]
                });
            }

            this._alertCtrl.create({
                title: 'Select Doctor',
                inputs: dCtrl,
                buttons: [{
                    text: 'Cancel'
                }, {
                    text: 'OK',
                    handler: data => {
                        showProfPage(data);
                    }
                }]
            }).present();
        } else {
            showProfPage(this.selectedDoctor);
        }
    }

    public patientAPT(apt: IPatientAppointment) {
        this.navCtrl.push(PatientProfilePage, {
            pc: apt.patient,
            apt: apt,
            node: this.node
        }).then(() => {
            this._events.subscribe('pProfile:close',
                this._clearPatientSearchBar.bind(this));
        });
    }

    public patientOpen(pc: IPatientCreds) {
        this.navCtrl.push(PatientProfilePage, { pc, apt: {}, node: this.node })
            .then(() => {
                this._events.subscribe('pProfile:close',
                    this._clearPatientSearchBar.bind(this));
            });
    }

}
