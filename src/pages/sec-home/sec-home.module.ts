import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecHomePage } from './sec-home';

@NgModule({
    declarations: [
        SecHomePage,
    ],
    imports: [
        IonicPageModule.forChild(SecHomePage),
    ],
})
export class SecHomePageModule { }
