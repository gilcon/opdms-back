import { AlertController, PopoverController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { StoreProvider } from '../../store/store';
import { LoggerProvider } from '../../logger/logger';
import { IReligion } from '../../../model/common';
import { AddRemovePopoverPage } from '../../../pages/popover/add-remove-popover/add-remove-popover';

@Injectable()
export class ReligionsSettingProvider {

    public religionList: IReligion[] = [];

    constructor(
        private _sp: StoreProvider,
        private _log: LoggerProvider,
        private _alertCtrl: AlertController,
        private _popOver: PopoverController
    ) {
    }

    public fetchReligions() {
        this.religionList = [];
        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .allDocs({
                include_docs: true,
                startkey: `settings:religion:`,
                endkey: `settings:religion:\ufff0`
            }).then(res => {
                let rows = res.rows;

                for (let i = 0; i < rows.length; i++) {
                    let doc = rows[i].doc;

                    this.religionList.push({
                        _id: doc._id,
                        _rev: doc._rev,
                        religion: doc.religion
                    });
                }
            }).catch(e => {
                this._log.log(e);
            });
    }

    public add(religion: string) {
        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .put({
                _id: `settings:religion:${religion}`,
                religion: religion
            }).then(res => {
                if (res.ok) {
                    this.religionList.push({
                        _id: res.id,
                        _rev: res.rev,
                        religion: religion
                    })
                }
            })
    }

    public remove(religions: IReligion[]) {
        for (let i = 0; i < religions.length; i++) {
            (religions as any)._deleted = true;
        }

        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .bulkDocs(religions).then(() => {
                this.fetchReligions();
            }).catch(e => {
                this._log.log(e);
            });
    }

    public alertAdd(arp: AddRemovePopoverPage) {
        this._alertCtrl.create({
            title: 'Add New Religion',
            inputs: [{
                name: 'religion',
                placeholder: 'Religion'
            }],
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    arp.viewCtrl.dismiss();
                }
            }, {
                text: 'Add',
                handler: data => {
                    this.add(data.religion);
                    arp.viewCtrl.dismiss();
                }
            }]
        }).present();
    }

    public alertRM(arp: AddRemovePopoverPage) {
        let deleted = this.religionList.filter(r => { return r['_deleted'] });
        let religions: string[] = [];

        for (let i = 0; i < deleted.length; i++) {
            religions.push(deleted[i].religion);
        }

        this._alertCtrl.create({
            title: 'Confirm Deletion',
            message: 'Are you sure to remove selected religions?<br>' +
                religions.join('<br>'),
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    arp.viewCtrl.dismiss();
                }
            }, {
                text: 'Confirm',
                handler: () => {
                    this.remove(deleted);
                    arp.viewCtrl.dismiss();
                }
            }]
        }).present();
    }

    public popOver(event) {
        this._popOver.create(AddRemovePopoverPage, {
            addCB: (arp: AddRemovePopoverPage) => {
                this.alertAdd(arp);
            },
            rmCB: (arp: AddRemovePopoverPage) => {
                this.alertRM(arp);
            }
        }).present({ ev: event });
    }

}
