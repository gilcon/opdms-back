import { AlertController, PopoverController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { ITitle } from '../../../model/common';
import { StoreProvider } from '../../store/store';
import { LoggerProvider } from '../../logger/logger';
import { AddRemovePopoverPage } from '../../../pages/popover/add-remove-popover/add-remove-popover';

@Injectable()
export class TitlesSettingProvider {

    public titleList: ITitle[] = [];

    constructor(
        private _sp: StoreProvider,
        private _log: LoggerProvider,
        private _alertCtrl: AlertController,
        private _popOver: PopoverController
    ) {
    }

    public fetchTitles() {
        this.titleList = [];
        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .allDocs({
                include_docs: true,
                startkey: 'settings:title:',
                endkey: 'settings:title:\ufff0'
            }).then(res => {
                let rows = res.rows;

                for (let i = 0; i < rows.length; i++) {
                    let doc = rows[i].doc;
                    this.titleList.push({
                        _id: doc._id,
                        _rev: doc._rev,
                        title: doc.title
                    });
                }
            }).catch(e => {
                this._log.log(e);
            });
    }

    public add(title: string) {
        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .put({
                _id: `settings:title:${title}`,
                title: title
            }).then(res => {
                if (res.ok) {
                    this.titleList.push({
                        _id: res.id,
                        _rev: res.rev,
                        title: title
                    });
                }
            }).catch(e => {
                this._log.log(e);
            });
    }

    public remove(titles: ITitle[]) {
        for (let i = 0; i < titles.length; i++) {
            (titles[i] as any)._deleted = true;
        }

        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .bulkDocs(titles).then(() => {
                this.fetchTitles();
            }).catch(e => {
                this._log.log(e);
            });
    }

    public alertAdd(arp: AddRemovePopoverPage) {
        this._alertCtrl.create({
            title: 'Add New Title',
            inputs: [{
                name: 'title',
                placeholder: 'Title'
            }],
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    arp.viewCtrl.dismiss();
                }
            }, {
                text: 'Add',
                handler: data => {
                    this.add(data.title);
                    arp.viewCtrl.dismiss();
                }
            }]
        }).present();
    }

    public alertRM(arp: AddRemovePopoverPage) {
        let deleted = this.titleList.filter(t => { return t['_deleted'] });
        let titles: string[] = [];

        for (let i = 0; i < deleted.length; i++) {
            titles.push(deleted[i].title);
        }

        this._alertCtrl.create({
            title: 'Confirm Deletion',
            message: 'Are you sure to remove selected titles?<br>' +
                titles.join('<br>'),
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    arp.viewCtrl.dismiss();
                }
            }, {
                text: 'Confirm',
                handler: () => {
                    this.remove(deleted);
                    arp.viewCtrl.dismiss();
                }
            }]
        }).present();
    }

    public popOver(event) {
        this._popOver.create(AddRemovePopoverPage, {
            addCB: (arp: AddRemovePopoverPage) => {
                this.alertAdd(arp);
            },
            rmCB: (arp: AddRemovePopoverPage) => {
                this.alertRM(arp);
            }
        }).present({ ev: event });
    }

}
