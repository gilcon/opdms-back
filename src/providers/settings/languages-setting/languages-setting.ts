import { AlertController, PopoverController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { StoreProvider } from '../../store/store';
import { LoggerProvider } from '../../logger/logger';
import { ILanguage } from '../../../model/common';
import { AddRemovePopoverPage } from '../../../pages/popover/add-remove-popover/add-remove-popover';

@Injectable()
export class LanguagesSettingProvider {

    public langList: ILanguage[] = [];

    constructor(
        private _sp: StoreProvider,
        private _log: LoggerProvider,
        private _alertCtrl: AlertController,
        private _popOver: PopoverController
    ) {
    }

    public fetchLang() {
        this.langList = [];
        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .allDocs({
                include_docs: true,
                startkey: `settings:lang:`,
                endkey: `settings:lang:\ufff0`
            }).then(res => {
                let rows = res.rows;

                for (let i = 0; i < rows.length; i++) {
                    let doc = rows[i].doc;

                    this.langList.push({
                        _id: doc._id,
                        _rev: doc._rev,
                        language: doc.language
                    });
                }
            }).catch(e => {
                this._log.log(e);
            });
    }

    public add(language: string) {
        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .put({
                _id: `settings:lang:${language}`,
                language: language
            }).then(res => {
                if (res.ok) {
                    this.langList.push({
                        _id: res.id,
                        _rev: res.rev,
                        language: language
                    })
                }
            }).catch(e => {
                this._log.log(e);
            });
    }

    public remove(language: ILanguage[]) {
        for (let i = 0; i < language.length; i++) {
            (language as any)._deleted = true;
        }

        this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii')
            .bulkDocs(language).then(() => {
                this.fetchLang();
            }).catch(e => {
                this._log.log(e);
            })
    }

    public alertAdd(arp: AddRemovePopoverPage) {
        this._alertCtrl.create({
            title: 'Add New Language',
            inputs: [{
                name: 'language',
                placeholder: 'Language'
            }],
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    arp.viewCtrl.dismiss();
                }
            }, {
                text: 'Add',
                handler: data => {
                    this.add(data.language);
                    arp.viewCtrl.dismiss();
                }
            }]
        }).present();
    }

    public alertRM(arp: AddRemovePopoverPage) {
        let deleted = this.langList.filter(l => { return l['_deleted'] });
        let languages: string[] = [];

        for (let i = 0; i < deleted.length; i++) {
            languages.push(deleted[i].language);
        }

        this._alertCtrl.create({
            title: 'Confirm Deletion',
            message: 'Are you sure to remove selected languages?<br>' +
                languages.join('<br>'),
            buttons: [{
                text: 'Cancel',
                handler: () => {
                    arp.viewCtrl.dismiss();
                }
            }, {
                text: 'Confirm',
                handler: () => {
                    this.remove(deleted)
                }
            }]
        }).present();
    }

    public popOver(event) {
        this._popOver.create(AddRemovePopoverPage, {
            addCB: (arp: AddRemovePopoverPage) => {
                this.alertAdd(arp);
            },
            rmCB: (arp: AddRemovePopoverPage) => {
                this.alertRM(arp);
            }
        }).present({ ev: event });
    }

}
