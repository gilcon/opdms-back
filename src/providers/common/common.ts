import { Injectable } from '@angular/core';
import { IDoctorInfo } from '../../model/doctor';

@Injectable()
export class CommonProvider {

    constructor() {
    }

    public emptyDoctor: IDoctorInfo = {
        doctor: {},
        pubKey: '',
        signature: '',
        name: 'All Doctors',
        peers: []
    }

    public toLocaleTimeString(date: Date | string): string {
        if (date instanceof Date) {
            return date.toLocaleTimeString('en-US');
        } else {
            return new Date(date).toLocaleTimeString('en-US');
        }
    }
}
