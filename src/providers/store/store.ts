import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import PouchDB from 'pouchdb';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import upsert from 'pouchdb-upsert';

import { ServerAddressProvider } from '../../providers/server-address/server-address';
import { IGetResponse } from '../../model/response';

@Injectable()
export class StoreProvider {

    private _stores: any = {};

    private _storeLocation: string[] = [];
    private _serverAddress: string[] = [];

    public serverUUID: string;

    constructor(
        private _sap: ServerAddressProvider,
        private _http: HttpClient,
        public events: Events
    ) {
        if (this._sap.isApp) {
            PouchDB.plugin(cordovaSqlitePlugin);
        }

        PouchDB.plugin(upsert);
    }

    public initMonitor() {
        this.monitorStore();
        this.monitorAddress();

        this.registerStore('18YZR9UJZ2nOYVQQzRR9wjWt9ii');
        this.registerStore('errlog');
        this.registerAddress(this._sap.getOriginAddress());
    }

    public fetchServerUUID(): Promise<string> {
        return new Promise((resolve, reject) => {
            this._http.get(this._sap.getOriginAddress() + '/server/uuid')
                .subscribe((res: IGetResponse) => {
                    if (res.successful) {
                        resolve(res.msg);
                    } else {
                        reject('unable to get server uuid');
                    }
                }, err => {
                    reject(err);
                })
        });
    }

    public get(name: string) {
        let s = this._stores[name];
        return !s ? this.registerStore(name) : s;
    }

    private monitorAddress() {
        this.events.subscribe('server:address', sa => {
            this.registerAddress(sa);
        });
    }

    private monitorStore() {
        this.events.subscribe('store:location', sl => {
            this.registerStore(sl);
        });
    }

    private registerAddress(sa) {
        if (!sa || this._serverAddress.indexOf(sa) > -1) {
            return;
        }

        this._serverAddress.push(sa);
        for (let i = 0; i < this._storeLocation.length; i++) {
            let name = this._storeLocation[i];

            let remote = new PouchDB(sa + '/store/' + name);
            this._stores[name].sync(remote, { live: true, retry: true });
        }
    }

    private registerStore(sl: string) {
        if (!sl || this._stores[sl]) {
            return;
        }

        let s = this._sap.isApp ? new PouchDB(sl, {
            adapter: 'cordova-sqlite'
        }) : new PouchDB(sl);

        this._stores[sl] = s
        this._storeLocation.push(sl);

        for (let i = 0; i < this._serverAddress.length; i++) {
            let sa = this._serverAddress[i];
            let remote = new PouchDB(sa + '/store/' + sl);
            s.sync(remote, { live: true, retry: true });
        }

        return s;
    }
}
