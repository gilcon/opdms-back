import { Injectable } from '@angular/core';
import * as moment from 'moment';

import { StoreProvider } from '../store/store';
import { LoggerProvider } from '../logger/logger';
import { EncryptGcmProvider } from '../encrypt-gcm/encrypt-gcm';
import { PatientProvider } from '../patient/patient';

import { IAppointmentList, IPatientAppointment, IAppointmentVitalSign } from '../../model/appointment'
import { IPatientCreds } from '../../model/patient';
import { IDoctorInfo } from '../../model/doctor';

@Injectable()
export class AppointmentProvider {

    public appointmentList: IAppointmentList[] = [];

    constructor(
        private _sp: StoreProvider,
        private _log: LoggerProvider,
        private _enc: EncryptGcmProvider,
        private _pt: PatientProvider,
    ) {
    }

    // will extract the patient id from the appointment id.
    private _extractPatientID(id: string): string {
        return id.split(':')[1];
    }

    public appointmentMin(avs: IAppointmentVitalSign): any {
        return {
            at: avs.arrivalTime,
            ss: avs.scheduleStatus,
            bp: avs.bloodPressure,
            p: avs.pulse,
            w: avs.weight,
            h: avs.height,
            t: avs.temp,
            rp: avs.resp,
            wt: avs.waist,
            hp: avs.hip,
            pc: avs.patientComplaint,
            apn: avs.appointmentNotes,
        }
    }

    public appointmentFromMin(avs: any): IAppointmentVitalSign {
        return {
            arrivalTime: avs.at,
            scheduleStatus: avs.ss,
            bloodPressure: avs.bp,
            pulse: avs.p,
            weight: avs.w,
            height: avs.h,
            temp: avs.t,
            resp: avs.rp,
            waist: avs.wt,
            hip: avs.hp,
            patientComplaint: avs.pc,
            appointmentNotes: avs.apn,
        }
    }

    private _buildPatientAppt(doc: any, dr: IDoctorInfo)
        : Promise<IPatientAppointment> {
        return new Promise<IPatientAppointment>(resolve => {
            let apt: IPatientAppointment = { _id: doc._id };
            let ptId: string = this._extractPatientID(apt._id);

            this._pt.getPatientCreds(ptId, dr).then((pc: IPatientCreds) => {
                apt.patient = pc;
                apt.appointment = this.appointmentFromMin(
                    JSON.parse(this._enc.decrypt(doc.p, pc.k)));

                resolve(apt);
            });
        });
    }

    public initData(m: moment.Moment, dr: IDoctorInfo, node: string) {
        let listAppointment = (apt: IPatientAppointment, dr: IDoctorInfo) => {
            for (let i = 0; i < this.appointmentList.length; i++) {
                if (this.appointmentList[i].dr.signature !== dr.signature) {
                    continue;
                }

                for (let j = 0; j < this.appointmentList[i].apt.length; j++) {
                    if (this.appointmentList[i].apt[j]._id === apt._id) {
                        this.appointmentList[i].apt[j] = apt;
                        return;
                    }
                }

                this.appointmentList[i].apt.push(apt);
                return;
            }

            this.appointmentList.push({
                dr: dr,
                apt: [apt]
            });
        };

        let sp = this._sp.get(dr.doctor.aps);
        sp.allDocs({
            include_docs: true,
            startkey: m.format('YYYY-MM-DD'),
            endkey: m.format('YYYY-MM-DD') + '\ufff0'
        }).then(res => {
            for (let i = 0; i < res.rows.length; i++) {
                let doc = res.rows[i].doc;
                if (doc.error || doc.node !== node) continue;

                this._buildPatientAppt(doc, dr)
                    .then((apt: IPatientAppointment) => {
                        listAppointment(apt, dr);
                    });
            }

            sp.changes({
                since: 'now',
                live: true,
                include_docs: true,
            }).on('change', doc => {
                if (doc.doc.node !== node) return;

                this._buildPatientAppt(doc.doc, dr)
                    .then((apt: IPatientAppointment) => {
                        listAppointment(apt, dr)
                    });
            }).on('error', err => {
                this._log.log(err);
            });
        }).catch(e => {
            this._log.log(e);
        })
    }

    public listAppointments(names: string[]): IAppointmentList[] {
        if (!names || names.length < 1) {
            return this.appointmentList;
        }

        let appointmentList: IAppointmentList[] = [];
        for (let i = 0; i < this.appointmentList.length; i++) {
            appointmentList.push({
                dr: this.appointmentList[i].dr,
                apt: this.appointmentList[i].apt
                    .filter((apt: IPatientAppointment): boolean => {
                        let name = apt.patient.patient.name;
                        return names.some((n: string): boolean => {
                            return name.first.includes(n) ||
                                name.last.includes(n) ||
                                name.middle.includes(n);
                        });
                    })
            });
        }

        return appointmentList;
    }

    public save(pa: IPatientAppointment, node: string): string {
        let m = moment();
        pa._id = pa._id || m.format('YYYY-MM-DD:') + pa.patient._id +
            m.format(':HH:mm:ss')

        this._sp.get(pa.patient.doctor.doctor.aps)
            .upsert(pa._id, doc => {
                let min: any = this.appointmentMin(pa.appointment)
                min = JSON.stringify(min)
                doc.p = this._enc.encrypt(min, pa.patient.k);
                doc.node = node;
                return doc;
            }).catch(e => {
                this._log.log(e);
            });

        return pa._id;
    }
}
