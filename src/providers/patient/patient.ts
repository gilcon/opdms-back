import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { StoreProvider } from '../store/store';
import { LoggerProvider } from '../logger/logger';
import { EncryptGcmProvider } from '../encrypt-gcm/encrypt-gcm';
import { UserProvider } from '../user/user';
import { v4 } from 'uuid';
import * as NodeRSA from 'node-rsa';
import ksuid from 'ksuid'

import { IDoctorInfo } from '../../model/doctor';
import { IPatient, IPatientCreds, IPatientPayload } from '../../model/patient';

@Injectable()
export class PatientProvider {

    constructor(
        private _sp: StoreProvider,
        private _log: LoggerProvider,
        private _enc: EncryptGcmProvider,
        private _user: UserProvider,
        private _events: Events
    ) {
    }

    public PatientMin(p: IPatient): any {
        return {
            t: p.title,
            sn: p.name.suffix,
            fn: p.name.first,
            ln: p.name.last,
            mn: p.name.middle,
            nn: p.name.nick,
            s: p.sex,
            ms: p.maritalStatus,
            bd: p.birthdate,
            l: p.language,
            r: p.religion,
            o: p.occupation,
            c: p.city,
            a: p.address,
            cm: p.contactMobile,
            ch: p.contactHome,
            ce: p.contactEmail,
            co: p.contactOffice,
            f: p.father,
            m: p.mother,
            g: p.guardian,
            rb: p.referredby,
            aly: p.allergies,
            reg: p.registration
        }
    }

    public PatientFromMin(p: any): IPatient {
        return {
            title: p.t,
            name: {
                suffix: p.sn,
                first: p.fn,
                last: p.ln,
                middle: p.mn,
                nick: p.nn,
            },
            sex: p.s,
            maritalStatus: p.ms,
            birthdate: p.bd,
            language: p.l,
            religion: p.r,
            occupation: p.o,
            city: p.c,
            address: p.a,
            contactMobile: p.cm,
            contactHome: p.ch,
            contactEmail: p.ce,
            contactOffice: p.co,
            father: p.f,
            mother: p.m,
            guardian: p.g,
            referredby: p.rb,
            allergies: p.aly,
            registration: p.reg,
        }
    }

    public patientNameString(pt: IPatient): string {
        return [pt.name.first, pt.name.middle, pt.name.last].join(' ');
    }

    private _decryptPatientInfo(
        pp: IPatientPayload,
        dr: IDoctorInfo,
        callback: (pc: IPatientCreds) => void): void {

        this.getPatientKey(pp._id, dr).then((key: string) => {
            let patient: any = this._enc.decrypt(pp.p, key);
            patient = JSON.parse(patient);
            patient = this.PatientFromMin(patient);
            callback({
                _id: pp._id,
                k: key,
                patient: patient,
                doctor: dr,
            })
        }).catch(e => {
            this._log.log(e);
            callback(null);
        });
    }

    public getPatientCreds(_id: string, dr: IDoctorInfo): Promise<IPatientCreds> {
        return new Promise<IPatientCreds>((resolve, reject) => {
            this._sp.get(dr.doctor.ps).get(_id).then(doc => {
                this._decryptPatientInfo({
                    _id: doc._id,
                    p: doc.p
                }, dr, (pc: IPatientCreds) => {
                    if (!pc) {
                        reject(null);
                    }

                    resolve(pc);
                })
            }).catch(e => {
                reject(e)
            });
        });
    }

    public getPatientKey(_id: string, dr: IDoctorInfo): Promise<string> {
        return new Promise<string>(resolve => {
            this._sp.get(dr.doctor.pes)
                .get(`${_id}${this._user.signature}`)
                .then((doc: any) => {
                    let pkBuf = Buffer.from(this._user.currentSession.privKey,
                        'base64');
                    let rsa = new NodeRSA(pkBuf, 'pkcs1-der');
                    resolve(rsa.decrypt(doc.p, 'utf8'));
                })
        });
    }

    public nameSearch(
        names: Array<string>,
        dr: IDoctorInfo,
        searchEventName: string
    ): void {
        let _indexSearchP = [];
        for (let i = 0; i < names.length; i++) {
            _indexSearchP.push(this._indexSearch(names[i], dr));
        }

        Promise.all(_indexSearchP).then(indexed => {
            let keys = [];
            for (let i = 0; i < indexed.length; i++) {
                for (let j = 0; j < indexed[i].length; j++) {
                    if (keys.includes(indexed[i][j])) continue;
                    keys.push(indexed[i][j]);
                }
            }

            this._sp.get(dr.doctor.ps).allDocs({
                include_docs: true,
                keys: keys
            }).then(res => {
                let rows = res.rows;
                for (let i = 0; i < rows.length; i++) {
                    let p = rows[i];
                    if (p.error) continue;

                    this._decryptPatientInfo({
                        _id: p.doc._id,
                        p: p.doc.p
                    }, dr, (pc: IPatientCreds) => {
                        if (!pc) return;
                        this._events.publish(searchEventName, dr, pc);
                    })
                }
            }).catch(e => {
                this._log.log(e)
            });
        });
    }

    private _indexSearch(name: string, dr: IDoctorInfo): Promise<string[]> {
        return new Promise<string[]>(resolve => {
            this._sp.get(dr.doctor.pti).allDocs({
                include_docs: true,
                startkey: name,
                endkey: `${name}\ufff0`
            }).then(res => {
                let r = res.rows;

                let rr = [];
                for (let i = 0; i < r.length; i++) {
                    if (r[i].error) continue;
                    rr.push(...r[i].doc.l);
                }

                resolve(rr);
            });
        });
    }

    private _upsertPTI(name: string, id: string, pti: any) {
        pti.upsert(name.toLowerCase(), doc => {
            if (!doc.l) {
                doc.l = [];
            }

            doc.l.push(id);
            return doc;
        }).catch(e => {
            this._log.log(e);
        })
    }

    public save(pc: IPatientCreds): string {
        let newPatient = !pc.k;
        pc._id = pc._id || ksuid.randomSync().string;
        pc.k = pc.k || v4();

        let ps = this._sp.get(pc.doctor.doctor.ps);
        let pti = this._sp.get(pc.doctor.doctor.pti);

        ps.upsert(pc._id, doc => {
            doc.p = this._enc.encrypt(
                JSON.stringify(this.PatientMin(pc.patient)), pc.k)
            return doc;
        }).then(res => {
            if (res.updated) {

                this._upsertPTI(pc.patient.name.first, pc._id, pti);
                this._upsertPTI(pc.patient.name.last, pc._id, pti);
                this._upsertPTI(pc.patient.name.middle, pc._id, pti);

                if (newPatient) {
                    let pes = this._sp.get(pc.doctor.doctor.pes);

                    for (let i = 0; i < pc.doctor.peers.length; i++) {
                        let pukBuf = Buffer.from(pc.doctor.peers[i].p, 'base64');
                        let rsa = new NodeRSA(pukBuf, 'pkcs8-public-der');

                        pes.put({
                            _id: `${pc._id}${pc.doctor.peers[i].s}`,
                            p: rsa.encrypt(pc.k, 'base64'),
                        })
                    }
                }
            }
        }).catch(e => {
            this._log.log(e);
        })

        return pc._id;
    }
}
