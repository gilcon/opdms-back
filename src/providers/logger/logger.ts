import { Injectable } from '@angular/core';
import ksuid from 'ksuid';
import * as moment from 'moment';

import { StoreProvider } from '../store/store';

@Injectable()
export class LoggerProvider {
    private _errLog: any;

    constructor(
        private _sp: StoreProvider
    ) {
        this._errLog = this._sp.get('errlog');
    }

    public log(e) {
        this._errLog.put({
            _id: ksuid.randomSync().string,
            t: moment().unix(),
            e: e.message
        })
    }
}
