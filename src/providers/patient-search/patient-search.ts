import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { v4 } from 'uuid';

import { PatientProvider } from '../patient/patient';
import { IPatientSearched, IPatientCreds } from '../../model/patient';
import { IDoctorInfo } from '../../model/doctor';
import { CommonProvider } from '../common/common';

@Injectable()
export class PatientSearchProvider {

    private _searchEventName: string = '';

    public searchTerms: string[] = [];
    public searchedPatients: IPatientSearched[] = [];

    constructor(
        private _pt: PatientProvider,
        private _events: Events,
        public common: CommonProvider
    ) {
    }

    public onSearch(ev: any, doctorList: IDoctorInfo[]) {
        this.searchTerms.length = 0;
        this.searchedPatients.length = 0;
        this._events.unsubscribe(this._searchEventName, this._searchEventHandler);

        if (!ev || !ev.target.value) return;

        let sv = ev.target.value.split(/\s+/);

        if (sv.length === 1 && sv[0].length < 3) {
            this.searchTerms.length = 0;
            return;
        }

        for (let i = 0; i < sv.length; i++) {
            if (sv[i].length > 2) {
                this.searchTerms.push(sv[i]);
            }
        }

        this._searchEventName = v4();

        this._events.subscribe(this._searchEventName,
            this._searchEventHandler.bind(this));

        for (let i = 0; i < doctorList.length; i++) {
            if (doctorList[i] === this.common.emptyDoctor) continue;

            this._pt.nameSearch(
                this.searchTerms,
                doctorList[i],
                this._searchEventName
            );
        }
    }

    public _searchEventHandler(dr: IDoctorInfo, pc: IPatientCreds) {
        // this will check if the IDoctorInfo is already set, and it will add the
        // IPatientCreds to the list will do an early return. Otherwise it will
        // push a new instance of IPatientSearched.
        for (let i = 0; i < this.searchedPatients.length; i++) {
            if (this.searchedPatients[i].dr.signature !== dr.signature) {
                continue;
            }

            this.searchedPatients[i].patients.push(pc);
            return
        }

        this.searchedPatients.push({
            dr: dr,
            patients: [pc]
        });
    }

}
