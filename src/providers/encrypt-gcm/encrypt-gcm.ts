import { Injectable } from '@angular/core';
import { createCipheriv, createDecipheriv, randomBytes, CipherGCMTypes, pbkdf2Sync } from 'crypto';

@Injectable()
export class EncryptGcmProvider {

    private _algo: CipherGCMTypes = 'aes-128-gcm';
    private _nonce_size: number = 16;
    private _tag_size: number = 16;
    private _key_size: number = 16;
    private _PBKDF2_name: string = 'sha256';
    private _PBKDF2_salt_size: number = 16;
    private _PBKDF2_iterations: number = 32767;

    constructor() {
    }

    private _encrypt(pt: Buffer, k: Buffer): Buffer {
        let n = randomBytes(this._nonce_size);
        let c = createCipheriv(this._algo, k, n);
        let ct = Buffer.concat([c.update(pt), c.final()]);

        return Buffer.concat([n, ct, c.getAuthTag()]);
    }

    private _decrypt(pt: Buffer, k: Buffer): Buffer {
        let n = pt.slice(0, this._nonce_size);
        let ct = pt.slice(this._nonce_size, pt.length - this._tag_size);
        let tag = pt.slice(ct.length + this._nonce_size);

        let c = createDecipheriv(this._algo, k, n);
        c.setAuthTag(tag);
        return Buffer.concat([c.update(ct), c.final()]);
    }

    public encrypt(plainText: string, key: string): string {
        plainText = plainText || '';
        key = key || '';
        let s = randomBytes(this._PBKDF2_salt_size);
        let k = pbkdf2Sync(Buffer.from(key, 'utf8'), s, this._PBKDF2_iterations, this._key_size, this._PBKDF2_name);
        let eSalt = Buffer.concat([s, this._encrypt(Buffer.from(plainText, 'utf8'), k)]);
        return eSalt.toString('base64');
    }

    public decrypt(payload: string, key: string): string {
        payload = payload || '';
        key = key || '';
        let pBuf = Buffer.from(payload, 'base64');
        let s = pBuf.slice(0, this._PBKDF2_salt_size);
        let cn = pBuf.slice(this._PBKDF2_salt_size);
        let k = pbkdf2Sync(Buffer.from(key, 'utf8'), s, this._PBKDF2_iterations, this._key_size, this._PBKDF2_name);

        return this._decrypt(cn, k).toString('utf8');
    }
}
