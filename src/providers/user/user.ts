import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { Events } from 'ionic-angular';

import 'rxjs/add/operator/map';

import { ServerAddressProvider } from '../../providers/server-address/server-address';
import { EncryptGcmProvider } from '../../providers/encrypt-gcm/encrypt-gcm';
import { StoreProvider } from '../../providers/store/store';
import { LoggerProvider } from '../../providers/logger/logger';

import { IPostResponse } from '../../model/response';

export enum UserType {
    ADMIN = 1,
    DOCTOR,
    SECRETARY
}

@Injectable()
export class UserProvider {

    public currentSession: any = {};
    public signature: string;
    public userType: UserType;

    public patientStore: any = {};
    public appointmentStore: any = {};

    // Variables for doctor only
    public visitStore: string;

    // Variables for secretary only

    constructor(
        private _serverAddress: ServerAddressProvider,
        private _enc: EncryptGcmProvider,
        private _localStorage: Storage,
        private _sp: StoreProvider,
        private _log: LoggerProvider,
        private _http: HttpClient,
        public events: Events
    ) {
    }

    public signalAuth() {
        this._localStorage.get('session').then(session => {
            if (!session) {
                this.events.publish('user:session', session);
                return;
            }

            session.act = parseInt(session.act);
            this.currentSession = session;
            this.signature = session.signature;
            this.userType = session.act;

            this.events.publish('user:session', session);
        });
    }


    public login(username: string, password: string) {
        return new Promise((resolve, reject) => {
            this._http.post(
                this._serverAddress.getOriginAddressAPI() + '/users/login',
                JSON.stringify({ username, password }), {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                }
            ).subscribe((res: IPostResponse) => {
                if (res.successful) {
                    this._localStorage.set('session', res.msg).then(() => {
                        this.signalAuth();
                    })
                }

                resolve(res.successful);
            }, err => {
                reject(err);
            });
        });
    }

    public addSecretary(username: string, password: string) {
        return new Promise(resolve => {
            this._http.post(
                this._serverAddress.getOriginAddressAPI() + '/users/login',
                JSON.stringify({ username, password }), {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                }
            ).subscribe((res: IPostResponse) => {
                if (!res.successful ||
                    parseInt(res.msg.act) !== UserType.SECRETARY) {
                    return resolve(null);
                }

                let sec = res.msg;
                let dr = this.currentSession;

                let secInfo = JSON.stringify({
                    address: sec.address,
                    contact: sec.contact,
                    name: sec.name
                });

                let secPayload = {
                    _id: `s${dr.signature}${sec.signature}`,
                    i: this._enc.encrypt(secInfo, dr.uuid),
                    s: sec.signature,
                    p: sec.pubKey
                }

                let drInfo = JSON.stringify({
                    address: dr.address,
                    contact: dr.contact,
                    name: dr.name,
                    pti: dr.pti,
                    ps: dr.ps,
                    aps: dr.aps,
                    pes: dr.pes
                });

                let docPayload = {
                    _id: `d${sec.signature}${dr.signature}`,
                    i: this._enc.encrypt(drInfo, sec.uuid),
                    s: dr.signature,
                    p: dr.pubKey
                };

                let s = this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii');

                s.get(`peers${dr.signature}`).then(doc => {
                    doc.peers.push({
                        s: sec.signature,
                        p: sec.pubKey
                    });

                    return doc;
                }).then(doc => {
                    return s.bulkDocs([
                        doc,
                        secPayload,
                        docPayload
                    ]);
                }).then(res => {
                    resolve(true);
                }).catch(e => {
                    this._log.log(e);
                    resolve(false);
                })
            });
        });
    }

    public removeSecretary(
        secList: Array<{ _id: string, _rev: string, pubKey: string }>
    ) {
        return new Promise(resolve => {
            let s = this._sp.get('18YZR9UJZ2nOYVQQzRR9wjWt9ii');

            for (let i = 0; i < secList.length; i++) {
                s.remove(secList[i]._id, secList[i]._rev).then(res => {
                    return s.get(`peers${this.signature}`);
                }).then(doc => {
                    doc.peers = doc.peers.filter((value, index, arr) => {
                        return value.p !== secList[i].pubKey;
                    });

                    return s.put(doc);
                }).then(res => {
                }).catch(e => {
                    this._log.log(e);
                });
            }
        });
    }

    public logout() {
        this._localStorage.set('session', null);
        this._localStorage.set('user:currentnode', null);
        this.events.publish('user:session', null);
    }

    public usernameExists(username: string) {
        return this._http.post(
            this._serverAddress.getRemoteAddressAPI() + '/users/usernameexists',
            JSON.stringify({ username }), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }
        );
    }

    public register(userInfo) {
        return this._http.post(
            this._serverAddress.getRemoteAddressAPI() + '/registration/register',
            JSON.stringify(userInfo), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }
        );
    }
}
