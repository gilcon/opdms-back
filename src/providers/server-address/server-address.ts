import { Injectable } from '@angular/core';

@Injectable()
export class ServerAddressProvider {

    private _remoteAddress: string = 'http://fedora-acer:3000';
    public isApp: boolean = (!document.URL.startsWith('http')
        || document.URL.startsWith('http://localhost:8080'));

    private _originAddress: string = this.isApp ? 'http://192.168.10.2:3000'
        : window.location.origin;

    private _customAddress: any = {};

    constructor() {
    }

    public getOriginAddress() {
        return this._originAddress;
    }

    public getOriginAddressAPI() {
        return this._originAddress + '/api';
    }

    public getOriginAddressStore() {
        return this._originAddress + '/store';
    }

    public setCustomAddress(name: string, address: string) {
        this._customAddress[name] = address;
    }

    public getCustomAddress(name) {
        return this._customAddress[name];
    }

    public getCustomAddressList() {
        return this._customAddress;
    }

    public getCustomAddressAPI(name) {
        return this._customAddress[name] + '/api';
    }

    public getCustomAddressStore(name) {
        return this._customAddress[name] + '/store';
    }

    public getRemoteAddress() {
        return this._remoteAddress;
    }

    public getRemoteAddressAPI() {
        return this._remoteAddress + '/api';
    }

    public getRemoteAddressStore() {
        return this._remoteAddress + '/store';
    }
}
